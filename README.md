Tutoriel CouchDB App avec AngularJS et erica
============================================
Dernière mise à jour: 2013/01/13


Introduction
------------
Le développement web a beaucoup évolué depuis que j'ai la tête enfouis
dans PHP et MySQL depuis 2000 et presqu'exclusivement Drupal depuis 2005.
Ici nous évaluerons [AngularJS] côté client pour le Javascript
et [CouchDB] côté serveur pour stocker nos données. Ensemble,
ces deux projets sont suffisants pour réaliser une application web, sans
PHP, sans Apache httpd et sans MySQL.

[AngularJS] et [CouchDB] s'utilisent séparément. Ils n'ont rien à voir l'un
avec l'autre. Il s'agit simplement de deux projets que j'ai décidé d'utiliser
ensemble.

###AngularJS vs jQuery###

Nous pouvont utiliser du bon vieux JavaScript, jQuery, YUI ou toute autre
librairie JavaScript pour interfacer avec CouchDB. [AngularJS] apporte
beaucoup de nouveautés, permettant entre autre d'utiliser le HTML pour nos
*templates* et lier l'affichage aux données automatiquement. Regardez ce
vidéo d'introduction pour saisir la différence:

* à [90 secondes avec jQuery][angular-hello-world-jquery]
* et à [140 secondes pour AngularJS][angular-hello-world]

Avec jQuery, nous avons besoin de quelques lignes de JavaScript pour lier
le formulaire d'entrée avec son affichage, tandis qu'avec AngularJS, il
n'y a aucune ligne de JavaScript à écrire puisque tout est déclaré dans le
HTML.

AngularJS permet aussi de créer ses propres balises (un profil utilisateur,
par exemple) et bien plus encore. Nous ne ferons qu'effleurer ses possibilités.

###Pourquoi CouchDB###

[CouchDB] va remplacer à la fois notre serveur web (httpd) et notre base
de données MySQL, mais il ne s'agit ni d'un serveur web générique (on ne
pourra pas y utliser le PHP par exemple) ni d'un serveur SQL.

CouchDB est un serveur de documents JSON utilisant une interface REST sur
HTTP ou HTTPS, au lieu d'un protocol binaire comme MySQL ou MongoDB. La
validation des données se fait côté serveur en JavaScript.

Contrairement au SQL:

* il n'y a pas de `JOIN`
* les requêtes se font par Map/Reduce
* les mises à jour créent toujours une nouvelle version du document
* il n'y a pas de schema uniforme (les documents d'une même table peuvent
  contenir différents attributs)
* la réplication *master-master* est une opération standard
 
Basé sur ces tutoriels/projets
------------------------------
[Angular Seed][angular-seed]
  ~ Seed project for angular apps.
[Angular Phonecat][angular-phonecat]
  ~ Tutorial on building an angular application.
[Angular Couchdb Demo][angular-couch-demo]
  ~ Angular demo as a couchapp.
  ~ Cette version date de deux ans et beaucoup de choses ont changé depuis,
    dont le passage de couchapp à [erica] pour déployer des couch apps.
[Angular CouchDB Erica][angular-couch-erica]
  ~ My updated Angular demo as a couchapp using erica.

Remarque sur la vitalité de CouchDB
-----------------------------------
Un truc qui m'inquiète un peu c'est que [planet-couchdb] n'a aucun article
après le mois d'août 2012. Par contre, les *mailing lists* sont toujours
très actives.

La suite
--------
* Yeoman et HTML5 Boilerplate
* Compass et Suzy

Colophon
--------
Ce document **markdown** est conforme à l'implementation de pandoc 1.9.4.2.
Le HTML se génère avec `pandoc --normalize README.md`. Autrement, les listes
de définitions ne seront pas générées correctement.

----

[angular-seed]: https://github.com/angular/angular-seed
[angular-phonecat]: https://github.com/angular/angular-phonecat
[angular-couch-demo]: https://github.com/daleharvey/angular-couch-demo
[angular-couch-erica]: https://github.com/millette/angular-couch-demo
[erica]: https://github.com/benoitc/erica
[planet-couchdb]: http://planet.couchdb.org/
[AngularJS]: http://angularjs.org/
[CouchDB]: http://couchdb.apache.org/
[angular-hello-world]: http://www.youtube.com/watch?v=uFTFsKmkQnQ#t=140s
[angular-hello-world-jquery]: http://www.youtube.com/watch?v=uFTFsKmkQnQ#t=90s
[yeoman]: http://yeoman.io/
[grunt]: http://gruntjs.com/
[h5bp]: http://html5boilerplate.com/
[suzy]: http://susy.oddbird.net/
[compass]: http://compass-style.org/
[coffeeScript]: http://coffeescript.org/
[twitter-bootstrap]: http://twitter.github.com/bootstrap/
[NodeJS]: http://nodejs.org/
[Testacular]: http://vojtajina.github.com/testacular/
[SockJS]: http://sockjs.org/
[javascript-beautifier]: http://jsbeautifier.org/
[JSLint]: http://www.jslint.com/
[JSHint]: http://www.jshint.com/
[bower]: http://twitter.github.com/bower/
[BrowserStack]: http://www.browserstack.com/
[Jasmine]: http://pivotal.github.com/jasmine/
[angular-sprout]: https://github.com/thedigitalself/angular-sprout

Liens à classer
---------------
* http://couchapp.org/page/getting-started
* http://couchapp.org/page/index
* http://couchapp.org/page/what-is-couchapp
* https://github.com/couchapp/couchapp
* https://github.com/couchapp/couchapp/blob/master/README.rst

* http://guide.couchdb.org/editions/1/en/managing.html
* https://github.com/daleharvey/angular-couch-demo
* https://github.com/angular/angular-seed
* https://github.com/millette/angular-couch-demo
* https://github.com/daleharvey/angular-couch-demo
* https://github.com/daleharvey/angular-couch-demo/commits/master
* https://github.com/daleharvey/angular-couch-demo/commit/f126626423e337fac4015c660997a07ef3fe9b2c
* https://github.com/angular/angular-phonecat

* http://daringfireball.net/projects/markdown/syntax#list
* https://gitorious.org/ecrire/angular-couch/commits/master
